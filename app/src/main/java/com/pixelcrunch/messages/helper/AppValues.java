package com.pixelcrunch.messages.helper;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by Sean on 16-03-08.
 */
public class AppValues {
	private static AppValues appValues;
	private SharedPreferences prefs;

	private AppValues(Context context) {
		prefs = getDefaultSharedPreferences(context);
	}

	public static void initialize(Context context) {
		if (appValues == null) {
			appValues = new AppValues(context);
		}
	}

	public static AppValues getInstance() {
		if (appValues == null) {
			throw new IllegalStateException("Must call initialize first");
		}
		return appValues;
	}

	//<editor-fold desc="Twilio">
	public void setAccountSid(String accountSid) {
		prefs.edit().putString(Constants.Preferences.TWILIO_ACCOUNT_SID, accountSid).apply();
	}

	public String getAccountSid() {
		return prefs.getString(Constants.Preferences.TWILIO_ACCOUNT_SID, null);
	}

	public void setAuthToken(String authToken) {
		prefs.edit().putString(Constants.Preferences.TWILIO_AUTH_TOKEN, authToken).apply();
	}

	public String getAuthToken() {
		return prefs.getString(Constants.Preferences.TWILIO_AUTH_TOKEN, null);
	}

	public void setTwilioNumber(String number) {
		prefs.edit().putString(Constants.Preferences.TWILIO_PHONE_NUMBER, number).apply();
	}

	public String getTwilioNumber() {
		return prefs.getString(Constants.Preferences.TWILIO_PHONE_NUMBER, null);
	}
	//</editor-fold>
}
