package com.pixelcrunch.messages.helper;

import android.content.Context;
import android.graphics.Color;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * Created by sean1 on 10/15/2017.
 */

public class ValueHelper {
	public static boolean isNotNullOrEmpty(String string) {
		return string != null && !string.isEmpty();
	}

	public static String normalizePhoneNumber(String phoneNumber) {
		if (phoneNumber != null) {
			phoneNumber = phoneNumber.replaceAll("[^0-9]+", "");
			if (phoneNumber.length() == 10) {
				phoneNumber = "1" + phoneNumber;
			}
			return "+" + phoneNumber;
		}

		return null;
	}

	public static boolean isValidNumber(String number) {
		return normalizePhoneNumber(number).length() == 12;
	}

	public static String formatPhoneNumber(String phoneNumber) {
		if (phoneNumber != null) {
			//Remove any special characters
			phoneNumber = phoneNumber.replaceAll("[^0-9]+", "");

			if (phoneNumber.length() > 10) {
				java.text.MessageFormat phoneNumberFormat = new java.text.MessageFormat("+{0} ({1}) {2}-{3}");
				String[] phoneNumberArray = {phoneNumber.substring(0, 1), phoneNumber.substring(1, 4), phoneNumber.substring(4, 7), phoneNumber.substring(7)};
				return phoneNumberFormat.format(phoneNumberArray);
			} else if (phoneNumber.length() > 6) {
				java.text.MessageFormat phoneNumberFormat = new java.text.MessageFormat("({0}) {1}-{2}");
				String[] phoneNumberArray = {phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6)};
				return phoneNumberFormat.format(phoneNumberArray);
			} else if (phoneNumber.length() > 5 && phoneNumber.length() < 7) {
				return "(" + phoneNumber.substring(0, 3) + ") " + phoneNumber.substring(3);
			} else {
				return phoneNumber;
			}
		} else {
			return phoneNumber;
		}
	}

	public static String getInitials(String fullName) {
		String initials = "";

		if (!isNotNullOrEmpty(fullName)) {
			return initials;
		}

		return String.valueOf(fullName.toUpperCase().charAt(0));
	}

	public static int getStatusBarHeight(Context context) {
		int result = 0;
		int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = context.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	public static int manipulateColor(int color, float factor) {
		int a = Color.alpha(color);
		int r = Math.round(Color.red(color) * factor);
		int g = Math.round(Color.green(color) * factor);
		int b = Math.round(Color.blue(color) * factor);
		return Color.argb(a,
				Math.min(r, 255),
				Math.min(g, 255),
				Math.min(b, 255));
	}

	//<editor-fold desc="Dates">
	public static String getPrettyDate(Date date, Context context) {
		String prettyDate = "";
		if (date == null) {
			return prettyDate;
		}

		DateTime eventDate = new DateTime(date);
		DateTime nowDate = DateTime.now();
		if (eventDate.isAfter(nowDate)) {
			return "Now";
		}
		Duration duration = new Interval(eventDate, nowDate).toDuration();

		boolean isToday = eventDate.withTimeAtStartOfDay().isEqual(nowDate.withTimeAtStartOfDay());
		boolean isThisWeek = duration.getStandardDays() <= 7 &&
				eventDate.getDayOfWeek() < nowDate.getDayOfWeek();

		if (isToday) {
			prettyDate = android.text.format.DateFormat.getTimeFormat(context).format(date);
		} else if (isThisWeek) {
			DateTimeFormatter thisWeekFormat = DateTimeFormat.forPattern("EEE");
			prettyDate = thisWeekFormat.print(eventDate);
		} else {
			DateTimeFormatter olderFormat = DateTimeFormat.forPattern("MMM dd");
			prettyDate = olderFormat.print(eventDate);
		}

		return prettyDate;
	}

	public static String getLowestDetailDate(Date date, Context context) {
		String prettyLongDate = "";
		if (date == null) {
			return prettyLongDate;
		}

		DateTime eventDate = new DateTime(date);
		DateTime nowDate = DateTime.now();
		if (eventDate.isAfter(nowDate)) {
			return "Now";
		}
		Duration duration = new Interval(eventDate, nowDate).toDuration();

		boolean isToday = eventDate.withTimeAtStartOfDay().isEqual(nowDate.withTimeAtStartOfDay());
		boolean isThisWeek = duration.getStandardDays() <= 7 &&
				eventDate.getDayOfWeek() < nowDate.getDayOfWeek();

		if (isToday) {
			prettyLongDate = android.text.format.DateFormat.getTimeFormat(context).format(date);
		} else if (isThisWeek) {
			String dateString = DateTimeFormat.forPattern("EEEE").print(eventDate);
			String timeString = android.text.format.DateFormat.getTimeFormat(context).format(date);
			prettyLongDate = String.format("%s %s", dateString, timeString);
		} else {
			String dateString = DateTimeFormat.forPattern("MMM dd").print(eventDate);
			String timeString = android.text.format.DateFormat.getTimeFormat(context).format(date);
			prettyLongDate = String.format("%s %s", dateString, timeString);
		}

		return prettyLongDate;
	}

	public static String getTopDate(Date date, Context context) {
		String prettyLongDate = "";
		if (date == null) {
			return prettyLongDate;
		}

		DateTime eventDate = new DateTime(date);
		DateTime nowDate = DateTime.now();
		if (eventDate.isAfter(nowDate)) {
			return "Now";
		}
		Duration duration = new Interval(eventDate, nowDate).toDuration();

		boolean isToday = eventDate.withTimeAtStartOfDay().isEqual(nowDate.withTimeAtStartOfDay());
		boolean isThisWeek = duration.getStandardDays() <= 7 &&
				eventDate.getDayOfWeek() < nowDate.getDayOfWeek();

		if (isToday) {
			prettyLongDate = android.text.format.DateFormat.getTimeFormat(context).format(date);
		} else if (isThisWeek) {
			String dateString = DateTimeFormat.forPattern("EEEE").print(eventDate);
			String timeString = android.text.format.DateFormat.getTimeFormat(context).format(date);
			prettyLongDate = String.format("%s • %s", dateString, timeString);
		} else {
			String dayString = DateTimeFormat.forPattern("EEEE").print(eventDate);
			String dateString = DateTimeFormat.forPattern("MMM dd").print(eventDate);
			String timeString = android.text.format.DateFormat.getTimeFormat(context).format(date);
			prettyLongDate = String.format("%s, %s • %s", dayString, dateString, timeString);
		}

		return prettyLongDate;
	}
	//</editor-fold>
}
