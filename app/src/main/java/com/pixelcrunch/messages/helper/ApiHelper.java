package com.pixelcrunch.messages.helper;

import android.support.annotation.NonNull;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.pixelcrunch.messages.BuildConfig;
import com.pixelcrunch.messages.endpoint.TwilioApi;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by sean on 2017-08-19.
 */

public class ApiHelper {
	private static ApiHelper api;
	private long byteUsage = 0;

	private Retrofit retrofit;
	private TwilioApi twilioApi;

	public ApiHelper() {
	}

	public static void initialize() {
		Timber.i("okHttp: Initialize InfiniteApi");
		api = new ApiHelper();
	}

	public static ApiHelper getInstance() {
		if (api == null) {
			throw new IllegalStateException("Must call initialize first");
		}
		return api;
	}

	Retrofit getRetrofit() {
		if (retrofit == null) {
			retrofit = createRetrofitInstance();
		}

		return retrofit;
	}

	public TwilioApi getApi() {
		if (twilioApi == null) {
			Timber.i("okHttp:twilioApiCreate");
			twilioApi = getRetrofit().create(TwilioApi.class);
		}

		return twilioApi;
	}

	public long getByteUsage() {
		long tempByteUsage = byteUsage;
		byteUsage = 0;

		return tempByteUsage;
	}

	private Retrofit createRetrofitInstance() {
		Timber.i("okHttp:createRetrofitInstance");

		Retrofit.Builder builder = new Retrofit.Builder()
				.baseUrl("https://api.twilio.com/2010-04-01/");

		OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

		clientBuilder.addNetworkInterceptor(loggingInterceptor);
		if (BuildConfig.DEBUG) {
			clientBuilder.addNetworkInterceptor(new StethoInterceptor());
		}

		builder.client(clientBuilder.build());
		return builder.build();
	}

	//<editor-fold desc="Interceptors">
	private Interceptor loggingInterceptor = new Interceptor() {
		@Override
		public Response intercept(@NonNull Chain chain) throws IOException {
			Request request = chain.request();
			long startNs = System.nanoTime();
			Response response;

			try {
				response = chain.proceed(request);
			} catch (Exception ignored) {
				throw ignored;
			}

			long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);

			ResponseBody responseBody = response.body();
			BufferedSource source = responseBody.source();
			source.request(Long.MAX_VALUE); // Buffer the entire body.
			Buffer buffer = source.buffer();

			Timber.tag("OkHttp").d("%s %s - %s ms - %s bytes", request.method(), request.url().toString(), tookMs, buffer.size());
			if ((response.code() < 200 || response.code() > 299)) {
				Timber.tag("OkHttp").e("%s %s - %s: %s", request.method(), request.url().toString(), response.code(), response.body() != null ? response.body().toString() : "No body");
			}

			byteUsage += buffer.size();
			return response;
		}
	};
	//</editor-fold>
}
