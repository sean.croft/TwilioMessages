package com.pixelcrunch.messages.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.pixelcrunch.messages.model.RealmContact;
import com.pixelcrunch.messages.view.ColorGenerator;
import com.pixelcrunch.messages.view.TextDrawable;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sean1 on 10/15/2017.
 */

public class ImageHelper {
	public static Drawable getInitialDrawable(String displayName, String address) {
		return TextDrawable.builder().buildRound(String.valueOf(ValueHelper.getInitials(displayName)).toUpperCase(), ColorGenerator.DEFAULT.getColor(address));
	}

	public static Drawable getCircleDrawable(String address) {
		return TextDrawable.builder().buildRound("", ColorGenerator.DEFAULT.getColor(address));
	}

	public static void setContactAvatar(ImageView imageView, RealmContact contact, String address) {
		Context context = imageView.getContext();
		if (contact == null) {
			imageView.setImageDrawable(MaterialDrawableBuilder.with(context).setIcon(MaterialDrawableBuilder.IconValue.ACCOUNT_CIRCLE).setColor(ColorGenerator.DEFAULT.getColor(address)).setSizeDp(48).build());
			imageView.setScaleType(ImageView.ScaleType.CENTER);
			return;
		}

		if (!ValueHelper.isNotNullOrEmpty(address)) {
			address = "";
		}
		Glide.with(context).load(contact.getPhotoUri()).error(ImageHelper.getInitialDrawable(contact.getDisplayName(), address)).bitmapTransform(new CropCircleTransformation(context)).into(imageView);
	}
}
