package com.pixelcrunch.messages.helper;

import com.pixelcrunch.messages.BuildConfig;

/**
 * Created by sean1 on 10/12/2017.
 */

public class Constants {
	public interface Value {
		int PERMISSION_REQUEST = 101;
	}

	public interface Action {
		String GET_ALL_DATA = BuildConfig.APPLICATION_ID + ".getAllData";
		String GET_MESSAGES = BuildConfig.APPLICATION_ID + ".getMessages";
		String GET_CONTACTS = BuildConfig.APPLICATION_ID + ".getContacts";
		String SEND_SMS = BuildConfig.APPLICATION_ID + ".sendSMS";
		String RECEIVE_SMS = BuildConfig.APPLICATION_ID + ".receiveSMS";
	}

	public interface Extra {
		String ADDRESS = BuildConfig.APPLICATION_ID + ".ADDRESS";
		String SUCCESS = BuildConfig.APPLICATION_ID + ".success";
		String ERROR = BuildConfig.APPLICATION_ID + ".errorMessage";
		String BODY = BuildConfig.APPLICATION_ID + ".body";
		String TO = BuildConfig.APPLICATION_ID + ".to";
		String PREP_SEND = BuildConfig.APPLICATION_ID + ".prepSend";
	}

	public interface SmsType {
		int SENT = 0;
		int RECEIVED = 1;
		int DRAFT = 2;
	}

	public interface Preferences {
		String TWILIO_ACCOUNT_SID = BuildConfig.APPLICATION_ID + ".twilioAccountSid";
		String TWILIO_AUTH_TOKEN = BuildConfig.APPLICATION_ID + ".twilioAuthToken";
		String TWILIO_PHONE_NUMBER = BuildConfig.APPLICATION_ID + ".twilioPhoneNumber";
	}
}
