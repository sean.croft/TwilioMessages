package com.pixelcrunch.messages.helper;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.exceptions.RealmMigrationNeededException;
import timber.log.Timber;

public class RealmHelper {
	//<editor-fold desc="Setup">
	public static Realm setupRealm(Context context) {
		try {
			return Realm.getDefaultInstance();
		} catch (NullPointerException | IllegalStateException e) {
			try {
				Timber.w("Need to initialize Realm");
				Realm.init(context);
				Realm.setDefaultConfiguration(getRealmConfiguration());
				return Realm.getDefaultInstance();
			} catch (RealmMigrationNeededException innerE) {
				return recreateRealm();
			}
		} catch (RealmMigrationNeededException e) {
			return recreateRealm();
		}
	}

	private static Realm recreateRealm() {
		Timber.e("Migration Needed, Clearing realm");
		Realm.deleteRealm(getRealmConfiguration());
		Realm.setDefaultConfiguration(getRealmConfiguration());
		return Realm.getDefaultInstance();
	}

	private static RealmConfiguration getRealmConfiguration() {
		return new RealmConfiguration.Builder()
				.schemaVersion(1)
				.deleteRealmIfMigrationNeeded()
				.build();
	}
	//</editor-fold>

	public static boolean isValid(RealmObject object) {
		return object != null && object.isValid();
	}
}