package com.pixelcrunch.messages.helper;

import com.github.tamir7.contacts.Contact;
import com.github.tamir7.contacts.Contacts;
import com.github.tamir7.contacts.Query;

import java.util.List;

/**
 * Created by sean1 on 10/15/2017.
 */

public class Utils {
	public static Contact getContactForAddress(String address) {
		Query query = Contacts.getQuery();
		query.whereEqualTo(Contact.Field.PhoneNormalizedNumber, address);
		List<Contact> contacts = query.find();
		return contacts.isEmpty() ? null : contacts.get(0);
	}
}
