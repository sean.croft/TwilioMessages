package com.pixelcrunch.messages.model;

import com.github.tamir7.contacts.Contact;
import com.pixelcrunch.messages.helper.ValueHelper;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sean1 on 10/12/2017.
 */

public class RealmContact extends RealmObject {
	@PrimaryKey private long id;
	private String displayName;
	private String photoUri;

	//<editor-fold desc="Constructor">
	public RealmContact() {
	}

	public RealmContact(Contact contact) {
		this.id = contact.getId();
		this.displayName = contact.getDisplayName();
		this.photoUri = contact.getPhotoUri();
	}
	//</editor-fold>

	//<editor-fold desc="Fetching">
	@Ignore
	private static final String[] sortFields = new String[]{"displayName", "id"};
	@Ignore
	private static final Sort[] sortOrder = new Sort[]{Sort.ASCENDING, Sort.DESCENDING};

	public static RealmResults<RealmContact> getContacts(Realm realm) {
		return realm.where(RealmContact.class).findAllSorted(sortFields, sortOrder);
	}

	public static RealmResults<RealmContact> getContacts(Realm realm, String searchText) {
		if (!ValueHelper.isNotNullOrEmpty(searchText)) {
			return getContacts(realm);
		}

		RealmQuery<RealmContact> query = realm.where(RealmContact.class)
				.contains("displayName", searchText, Case.INSENSITIVE);

		String numberToSearch = searchText.replaceAll("[^0-9]+", "");
		if (ValueHelper.isNotNullOrEmpty(numberToSearch)) {
			RealmResults<RealmPhoneNumber> numbers = realm.where(RealmPhoneNumber.class)
					.contains("number", numberToSearch, Case.INSENSITIVE)
					.findAll();

			for (int i = 0; i < numbers.size(); i++) {
				query.or().equalTo("id", numbers.get(i).getContactId());
			}
		}

		return query.findAllSorted(sortFields, sortOrder);
	}

	public static RealmContact getContactWithNumber(Realm realm, String number) {
		RealmPhoneNumber phone = realm.where(RealmPhoneNumber.class).equalTo("number", number).findFirst();
		if (phone == null) {
			return null;
		}

		return realm.where(RealmContact.class).equalTo("id", phone.getContactId()).findFirst();
	}
	//</editor-fold>

	public RealmResults<RealmPhoneNumber> getNumbers(Realm realm) {
		return realm.where(RealmPhoneNumber.class).equalTo("contactId", getId()).findAllSorted("number", Sort.DESCENDING);
	}

	//<editor-fold desc="Getter and Setter">
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPhotoUri() {
		return photoUri;
	}

	public void setPhotoUri(String photoUri) {
		this.photoUri = photoUri;
	}
	//</editor-fold>
}
