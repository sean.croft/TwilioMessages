package com.pixelcrunch.messages.model;

import android.database.Cursor;
import android.telephony.SmsMessage;

import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.ValueHelper;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sean1 on 10/12/2017.
 */

public class Message extends RealmObject {
	@PrimaryKey private String id;
	private Date date;

	private Integer smsType;
	private String address;

	private String body;

	//<editor-fold desc="Constructor">
	public Message() {
	}

	public Message(Cursor cursor, int smsType) {
		this.smsType = smsType;
		try {
			int dateIndex = cursor.getColumnIndex("date_sent");
			int addressIndex = cursor.getColumnIndex("ADDRESS");
			int bodyIndex = cursor.getColumnIndex("body");

			this.date = dateIndex == -1 ? new Date() : new Date(cursor.getLong(dateIndex));
			this.address = addressIndex == -1 ? "" : ValueHelper.normalizePhoneNumber(cursor.getString(addressIndex));
			this.body = bodyIndex == -1 ? "" : cursor.getString(bodyIndex);
			sortOutAddress();

			this.id = String.format("%s%s", this.address, this.date.getTime());
		} catch (Exception ignored) {
			this.id = UUID.randomUUID().toString();
		}
	}

	public Message(SmsMessage smsMessage) {
		this.smsType = Constants.SmsType.RECEIVED;
		try {
			this.date = new Date(smsMessage.getTimestampMillis());
			this.address = ValueHelper.normalizePhoneNumber(smsMessage.getOriginatingAddress());
			this.body = smsMessage.getMessageBody();

			sortOutAddress();

			this.id = String.format("%s%s", this.address, this.date.getTime());
		} catch (Exception ignored) {
			this.id = UUID.randomUUID().toString();
		}
	}
	//</editor-fold>

	//<editor-fold desc="Helpers">
	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", date=" + date +
				", smsType=" + smsType +
				", ADDRESS='" + address + '\'' +
				", body='" + body + '\'' +
				'}';
	}

	private void sortOutAddress() {
		if (!ValueHelper.isNotNullOrEmpty(body)) {
			return;
		}

		try {
			Matcher m = Pattern.compile("\\~(.*?)\\~(?!\\s*\\~)\\s*", Pattern.DOTALL).matcher(body);
			while (m.find()) {
				String number = m.group(1);
				if (!number.contains("+")) {
					continue;
				}

				address = ValueHelper.normalizePhoneNumber(number);
				body = body.substring(number.length() + 2);
			}
		} catch (Exception ignored) {
		}
	}

	public String getPreview() {
		return smsType == Constants.SmsType.SENT ? String.format("You: %s", getBody().replaceAll("\n", " ")) : getBody().replaceAll("\n", " ");
	}

	public boolean fromSameSender(Message otherMessage) {
		if (otherMessage == null || otherMessage.getSmsType() == null || getSmsType() == null) {
			return false;
		}

		return Objects.equals(otherMessage.getSmsType(), getSmsType());
	}

	public boolean isWithinTime(Message otherMessage, long milliSeconds) {
		if (otherMessage == null || otherMessage.getDate() == null || getDate() == null) {
			return false;
		}

		long otherTime = otherMessage.getDate().getTime();
		long thisTime = getDate().getTime();

		if (otherTime > thisTime) {
			return otherTime - thisTime < milliSeconds;
		} else {
			return thisTime - otherTime < milliSeconds;
		}
	}

	public Message getPreviousMessage(Realm realm) {
		return getMessagesForAddress(realm, getAddress()).where().lessThan("date", getDate()).findFirst();
	}
	//</editor-fold>

	//<editor-fold desc="Fetching">
	@Ignore
	private static final String[] sortFields = new String[]{"date", "body"};
	@Ignore
	private static final Sort[] sortOrder = new Sort[]{Sort.DESCENDING, Sort.ASCENDING};

	public static RealmResults<Message> getDistinctSenderInbox(Realm realm) {
		return realm.where(Message.class)
				.findAllSorted(sortFields, sortOrder)
				.where()
				.distinct("address");
	}

	public static RealmResults<Message> getMessagesForAddress(Realm realm, String address) {
		return realm.where(Message.class)
				.equalTo("address", address)
				.findAllSorted(sortFields, sortOrder);
	}
	//</editor-fold>

	//<editor-fold desc="Getters and Setters">
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getSmsType() {
		return smsType;
	}

	public void setSmsType(Integer smsType) {
		this.smsType = smsType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	//</editor-fold>
}
