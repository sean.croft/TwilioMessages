package com.pixelcrunch.messages.model;

import com.github.tamir7.contacts.PhoneNumber;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sean1 on 10/12/2017.
 */

public class RealmPhoneNumber extends RealmObject {
	@PrimaryKey private String number;
	private long contactId;

	//<editor-fold desc="Constructor">
	public RealmPhoneNumber() {
	}

	public RealmPhoneNumber(long contactId, PhoneNumber phoneNumber) {
		this.contactId = contactId;
		this.number = phoneNumber.getNormalizedNumber();
	}
	//</editor-fold>

	//<editor-fold desc="Getter and Setter">
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	//</editor-fold>
}
