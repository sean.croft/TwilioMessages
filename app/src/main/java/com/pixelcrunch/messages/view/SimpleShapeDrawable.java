package com.pixelcrunch.messages.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;

/**
 * Used to create a {@link Bitmap} that contains a letter used in the English
 * alphabet or digit, if there is no letter or digit available, a default image
 * is shown instead
 */
public class SimpleShapeDrawable extends ShapeDrawable {
	private final Paint borderPaint;
	private static final float SHADE_FACTOR = 0.9f;
	private final int color;
	private final RectShape shape;
	private final int height;
	private final int width;
	private final float radius;
	private final int borderThickness;

	private SimpleShapeDrawable(Builder builder) {
		super(builder.shape);

		// shape properties
		shape = builder.shape;
		height = builder.height;
		width = builder.width;
		radius = builder.radius;

		// text and color
		color = builder.color;

		// border paint settings
		borderThickness = builder.borderThickness;
		borderPaint = new Paint();
		borderPaint.setColor(getDarkerShade(color));
		borderPaint.setStyle(Paint.Style.STROKE);
		borderPaint.setStrokeWidth(borderThickness);

		// drawable paint color
		Paint paint = getPaint();
		paint.setColor(color);

	}

	private int getDarkerShade(int color) {
		return Color.rgb((int) (SHADE_FACTOR * Color.red(color)),
				(int) (SHADE_FACTOR * Color.green(color)),
				(int) (SHADE_FACTOR * Color.blue(color)));
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		Rect r = getBounds();


		// draw border
		if (borderThickness > 0) {
			drawBorder(canvas);
		}

		int count = canvas.save();
		canvas.translate(r.left, r.top);
		canvas.restoreToCount(count);
	}

	private void drawBorder(Canvas canvas) {
		RectF rect = new RectF(getBounds());
		rect.inset(borderThickness / 2, borderThickness / 2);

		if (shape instanceof OvalShape) {
			canvas.drawOval(rect, borderPaint);
		} else if (shape instanceof RoundRectShape) {
			canvas.drawRoundRect(rect, radius, radius, borderPaint);
		} else {
			canvas.drawRect(rect, borderPaint);
		}
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	@Override
	public int getIntrinsicWidth() {
		return width;
	}

	@Override
	public int getIntrinsicHeight() {
		return height;
	}

	public static IShapeBuilder builder() {
		return new Builder();
	}

	public static class Builder implements IConfigBuilder, IShapeBuilder, IBuilder {

		private int color;
		private int borderThickness;
		private int width;
		private int height;
		private RectShape shape;
		public float radius;

		private Builder() {
			color = Color.GRAY;
			borderThickness = 0;
			width = -1;
			height = -1;
			shape = new RectShape();
		}

		public IConfigBuilder width(int width) {
			this.width = width;
			return this;
		}

		public IConfigBuilder height(int height) {
			this.height = height;
			return this;
		}

		public IConfigBuilder withBorder(int thickness) {
			this.borderThickness = thickness;
			return this;
		}

		@Override
		public IConfigBuilder beginConfig() {
			return this;
		}

		@Override
		public IShapeBuilder endConfig() {
			return this;
		}

		@Override
		public IBuilder rect() {
			this.shape = new RectShape();
			return this;
		}

		@Override
		public IBuilder round() {
			this.shape = new OvalShape();
			return this;
		}

		@Override
		public IBuilder roundRect(int radius) {
			this.radius = radius;
			float[] radii = {radius, radius, radius, radius, radius, radius, radius, radius};
			this.shape = new RoundRectShape(radii, null, null);
			return this;
		}

		@Override
		public SimpleShapeDrawable buildRect(int color) {
			rect();
			return build(color);
		}

		@Override
		public SimpleShapeDrawable buildRoundRect(int color, int radius) {
			roundRect(radius);
			return build(color);
		}

		@Override
		public SimpleShapeDrawable buildRound(int color) {
			round();
			return build(color);
		}

		@Override
		public SimpleShapeDrawable build(int color) {
			this.color = color;
			return new SimpleShapeDrawable(this);
		}
	}

	public interface IConfigBuilder {
		public IConfigBuilder width(int width);

		public IConfigBuilder height(int height);

		public IConfigBuilder withBorder(int thickness);

		public IShapeBuilder endConfig();
	}

	public static interface IBuilder {
		public SimpleShapeDrawable build(int color);
	}

	public static interface IShapeBuilder {

		public IConfigBuilder beginConfig();

		public IBuilder rect();

		public IBuilder round();

		public IBuilder roundRect(int radius);

		public SimpleShapeDrawable buildRect(int color);

		public SimpleShapeDrawable buildRoundRect(int color, int radius);

		public SimpleShapeDrawable buildRound(int color);
	}
}