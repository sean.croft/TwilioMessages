package com.pixelcrunch.messages.view;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Sean on 16-03-11.
 */
public class ColorGenerator {
	public static ColorGenerator DEFAULT;

	public static ColorGenerator MATERIAL;

	static {
		DEFAULT = create(Arrays.asList(
				0xffF44336,
				0xff9C27B0,
				0xff673AB7,
				0xff3F51B5,
				0xff2196F3,
				0xff039BE5,
				0xff0097A7,
				0xff009688,
				0xff43A047,
				0xff689F38,
				0xffEF6C00,
				0xffFF5722,
				0xffA1887F
		));
		MATERIAL = create(Arrays.asList(
				0xffe57373,
				0xfff06292,
				0xffba68c8,
				0xff9575cd,
				0xff7986cb,
				0xff64b5f6,
				0xff4fc3f7,
				0xff4dd0e1,
				0xff4db6ac,
				0xff81c784,
				0xffaed581,
				0xffff8a65,
				0xffd4e157,
				0xffffd54f,
				0xffffb74d,
				0xffa1887f,
				0xff90a4ae
		));
	}

	private final List<Integer> mColors;
	private final Random mRandom;

	public static ColorGenerator create(List<Integer> colorList) {
		return new ColorGenerator(colorList);
	}

	private ColorGenerator(List<Integer> colorList) {
		mColors = colorList;
		mRandom = new Random(System.currentTimeMillis());
	}

	public int getRandomColor() {
		return mColors.get(mRandom.nextInt(mColors.size()));
	}

	public int getColor(Object key) {
		return mColors.get(Math.abs(key.hashCode()) % mColors.size());
	}
}