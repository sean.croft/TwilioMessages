package com.pixelcrunch.messages.endpoint;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import com.pixelcrunch.messages.helper.ApiHelper;
import com.pixelcrunch.messages.helper.AppValues;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.model.Message;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

@SuppressWarnings("ConstantConditions")
public class TwilioApiService extends IntentService {
	private static HashSet<String> currentRequests = new HashSet<>();

	public TwilioApiService() {
		super("TwilioApiService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent == null || intent.getAction() == null) {
			return;
		}

		switch (intent.getAction()) {
			case Constants.Action.SEND_SMS:
				sendMessage(intent);
				break;
		}
	}

	//<editor-fold desc="Helpers">
	private boolean addUrlToOngoingRequests(Call<?> call) {
		if (call == null) {
			return false;
		}

		if (!call.request().method().equalsIgnoreCase("get")) {
			//only add get requests to list,
			//everything else should perform as expected
			return true;
		}

		String url = call.request().url().url().getPath();
		if (currentRequests.contains(url)) {
			Timber.d("networkRequest: request is already queued - %s", url);
			return false;
		}

		currentRequests.add(url);
		return true;
	}

	private void removeUrlFromOngoingRequests(Call<?> call) {
		if (call == null || call.request() == null || call.request().url() == null || call.request().url().url().getPath() == null) {
			return;
		}

		if (!call.request().method().equalsIgnoreCase("get")) {
			//only add get requests to list,
			//everything else should perform as expected
			return;
		}

		String url = call.request().url().url().getPath();
		if (currentRequests.contains(url)) {
			currentRequests.remove(url);
		}
	}

	private boolean requestIsOngoing(Call<?> call) {
		if (call == null || call.request() == null || call.request().url() == null || call.request().url().url().getPath() == null) {
			return false;
		}

		String url = call.request().url().url().getPath();
		return currentRequests.contains(url);
	}

	public String checkResponseForError(Response<?> response) {
		String error = null;

		if (response.raw().code() < 200 || response.raw().code() > 299) {
			try {
				error = response.errorBody().string();
			} catch (IOException e) {
				error = response.raw().message();
			}
		}

		return error;
	}
	//</editor-fold>

	private void sendMessage(Intent intent) {
		final String body = intent.getStringExtra(Constants.Extra.BODY);
		final String to = intent.getStringExtra(Constants.Extra.TO);

		String base64EncodedCredentials = "Basic " + Base64.encodeToString((AppValues.getInstance().getAccountSid() + ":" + AppValues.getInstance().getAuthToken()).getBytes(), Base64.NO_WRAP);

		final Map<String, String> data = new HashMap<>();
		data.put("From", AppValues.getInstance().getTwilioNumber());
		data.put("To", to);
		data.put("Body", body);

		Call<ResponseBody> feedCall = ApiHelper.getInstance().getApi().sendMessage(AppValues.getInstance().getAccountSid(), base64EncodedCredentials, data);

		if (addUrlToOngoingRequests(feedCall)) {
			Timber.d("feedCall");
			feedCall.enqueue(new Callback<ResponseBody>() {
				@Override
				public void onResponse(@NonNull Call<ResponseBody> call, @NonNull final Response<ResponseBody> response) {
					String error = checkResponseForError(response);
					if (error != null) {
						onFailure(call, new Throwable(error));
						return;
					}

					removeUrlFromOngoingRequests(call);

					try (Realm realm = RealmHelper.setupRealm(getApplicationContext())) {
						realm.executeTransactionAsync(new Realm.Transaction() {
							@Override
							public void execute(Realm realm) {
								Date date = new Date();
								Message message = new Message();
								message.setAddress(to);
								message.setBody(body);
								message.setDate(date);
								message.setId(String.format("%s%s", AppValues.getInstance().getTwilioNumber(), date.getTime()));
								message.setSmsType(Constants.SmsType.SENT);

								realm.copyToRealmOrUpdate(message);
							}
						}, new Realm.Transaction.OnSuccess() {
							@Override
							public void onSuccess() {
								Intent successIntent = new Intent(Constants.Action.SEND_SMS);
								successIntent.putExtra(Constants.Extra.SUCCESS, true);
								LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(successIntent);
							}
						});
					}
				}

				@Override
				public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
					removeUrlFromOngoingRequests(call);
					Intent failedIntent = new Intent(Constants.Action.SEND_SMS);
					failedIntent.putExtra(Constants.Extra.SUCCESS, false);
					failedIntent.putExtra(Constants.Extra.ERROR, t.getMessage());
					LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(failedIntent);
				}
			});
		}
	}
}
