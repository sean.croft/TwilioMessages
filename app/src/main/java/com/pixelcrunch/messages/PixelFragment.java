package com.pixelcrunch.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.pixelcrunch.messages.helper.RealmHelper;

import io.realm.Realm;

/**
 * Created by sean on 2017-08-19.
 */

public class PixelFragment extends Fragment {
	protected Realm realm;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		realm = RealmHelper.setupRealm(getActivity());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		realm.close();
	}
}
