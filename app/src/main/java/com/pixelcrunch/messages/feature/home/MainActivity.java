package com.pixelcrunch.messages.feature.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.pixelcrunch.messages.PixelActivity;
import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.feature.contactSelection.ContactSelectionActivity;
import com.pixelcrunch.messages.feature.settings.SettingsActivity;
import com.pixelcrunch.messages.feature.thread.ThreadDetailActivity;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.model.Message;
import com.pixelcrunch.messages.service.DataService;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends PixelActivity {
	//<editor-fold desc="View Initialization">
	@BindView(R.id.toolbar) Toolbar toolbar;

	@BindView(R.id.recyclerView) RecyclerView recyclerView;
	@BindView(R.id.fab) FloatingActionButton fab;
	//</editor-fold>

	MessageListAdapter adapter;

	//<editor-fold desc="Lifecycle">
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		setupToolbar();
		setupViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		getPermissions();
		setupRecyclerView();
	}
	//</editor-fold>

	private void setupToolbar() {
		setSupportActionBar(toolbar);
	}

	private void setupViews() {
		fab.setImageDrawable(MaterialDrawableBuilder.with(this).setIcon(MaterialDrawableBuilder.IconValue.PLUS).setColorResource(android.R.color.white).setSizeDp(24).build());
	}

	//<editor-fold desc="List">
	private void setupRecyclerView() {
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		adapter = new MessageListAdapter(Message.getDistinctSenderInbox(realm), realm, true, listListener);
		recyclerView.setAdapter(adapter);
	}
	//</editor-fold>

	//<editor-fold desc="Permissions">
	private static final String[] permissions = new String[]{
			Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS, Manifest.permission.RECEIVE_SMS
	};

	public void getPermissions() {
		boolean requiresPermission = false;
		for (String permission : permissions) {
			if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
				requiresPermission = true;

				if (shouldShowRequestPermissionRationale(permission)) {
					Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}

		if (requiresPermission) {
			requestPermissions(permissions, Constants.Value.PERMISSION_REQUEST);
		} else {
			getPhoneMessages();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == Constants.Value.PERMISSION_REQUEST) {
			boolean granted = true;
			for (int result : grantResults) {
				if (result != PackageManager.PERMISSION_GRANTED) {
					granted = false;
					break;
				}
			}

			if (granted) {
				Toast.makeText(this, "Read Action permission granted", Toast.LENGTH_SHORT).show();
				getPhoneMessages();
			} else {
				Toast.makeText(this, "Read Action permission denied", Toast.LENGTH_SHORT).show();
			}
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
	//</editor-fold>

	private void getPhoneMessages() {
		Intent intent = new Intent(MainActivity.this, DataService.class);
		intent.setAction(Constants.Action.GET_ALL_DATA);
		startService(intent);
	}

	//<editor-fold desc="Option Menu">
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			startActivity(new Intent(MainActivity.this, SettingsActivity.class));
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	//</editor-fold>

	//<editor-fold desc="Listeners">
	@OnClick(R.id.fab)
	public void onFabClicked() {
		startActivity(new Intent(MainActivity.this, ContactSelectionActivity.class));
	}

	MessageListAdapter.Listener listListener = new MessageListAdapter.Listener() {
		@Override
		public void onClicked(String address) {
			Intent intent = new Intent(MainActivity.this, ThreadDetailActivity.class);
			intent.putExtra(Constants.Extra.ADDRESS, address);
			startActivity(intent);
		}
	};
	//</editor-fold>
}
