package com.pixelcrunch.messages.feature.thread;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pixelcrunch.messages.PixelActivity;
import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.endpoint.TwilioApiService;
import com.pixelcrunch.messages.helper.AppValues;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.ValueHelper;
import com.pixelcrunch.messages.model.Message;
import com.pixelcrunch.messages.model.RealmContact;
import com.pixelcrunch.messages.view.ColorGenerator;

import net.steamcrafted.materialiconlib.MaterialIconView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by sean1 on 10/15/2017.
 */

public class ThreadDetailActivity extends PixelActivity {
	//<editor-fold desc="View Initialization">
	@BindView(R.id.toolbar) Toolbar toolbar;

	@BindView(R.id.recyclerView) RecyclerView recyclerView;
	@BindView(R.id.editText) EditText editText;
	@BindView(R.id.sendButton) MaterialIconView sendButton;
	@BindView(R.id.progressBar) ProgressBar progressBar;
	//</editor-fold>

	MessagesAdapter adapter;
	String address;
	RealmContact contact;

	int contactColor;

	//<editor-fold desc="Lifecycle">
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thread_detail);
		ButterKnife.bind(this);

		address = getAddress();
		if (!ValueHelper.isNotNullOrEmpty(address)) {
			Toast.makeText(this, "Unable to load messages", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		contact = RealmContact.getContactWithNumber(realm, address);

		setupToolbar();
		setupView();
		setupRecyclerView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerUpdateReceiver();
	}

	@Override
	protected void onPause() {
		unregisterUpdateReceiver();
		super.onPause();
	}
	//</editor-fold>

	private String getAddress() {
		Intent intent = getIntent();
		if (intent == null) {
			return null;
		}

		if (intent.getBooleanExtra(Constants.Extra.PREP_SEND, false)) {
			editText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					editText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
					editText.requestFocus();
				}
			});
		}

		return intent.getStringExtra(Constants.Extra.ADDRESS);
	}

	private void setupToolbar() {
		setSupportActionBar(toolbar);
		if (getSupportActionBar() == null) {
			return;
		}

		contactColor = ColorGenerator.DEFAULT.getColor(address);
		toolbar.setBackgroundColor(contactColor);
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		window.setStatusBarColor(ValueHelper.manipulateColor(contactColor, 0.67f));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(contact == null ? ValueHelper.formatPhoneNumber(address) : contact.getDisplayName());
	}

	private void setupView() {
		sendButton.setColor(contactColor);
		progressBar.getIndeterminateDrawable().setColorFilter(contactColor, android.graphics.PorterDuff.Mode.MULTIPLY);

	}

	private void showProgressBar(boolean show) {
		progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
		sendButton.setVisibility(show ? View.INVISIBLE : View.VISIBLE);

		sendButton.setEnabled(!show);
	}

	//<editor-fold desc="List">
	private void setupRecyclerView() {
		LinearLayoutManager manager = new LinearLayoutManager(this);
		manager.setReverseLayout(true);
		recyclerView.setLayoutManager(manager);
		adapter = new MessagesAdapter(Message.getMessagesForAddress(realm, address), contact, realm, true, listListener);
		recyclerView.setAdapter(adapter);
	}
	//</editor-fold>

	//<editor-fold desc="Receivers">
	private void registerUpdateReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.Action.SEND_SMS);
		filter.addAction(Constants.Action.RECEIVE_SMS);

		LocalBroadcastManager.getInstance(this).registerReceiver(updateReceiver, filter);
	}

	private void unregisterUpdateReceiver() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(updateReceiver);
	}

	private BroadcastReceiver updateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Timber.d("updateReceiver");
			String action = intent.getAction();
			if (action == null) {
				Timber.d("updateReceiver: action null");
				return;
			}

			switch (action) {
				case Constants.Action.SEND_SMS:
					showProgressBar(false);
					if (!intent.getBooleanExtra(Constants.Extra.SUCCESS, true)) {
						Snackbar.make(recyclerView, "Failed to send message", Snackbar.LENGTH_LONG).show();
						return;
					}

					editText.setText("");
					recyclerView.smoothScrollToPosition(0);
					break;
				case Constants.Action.RECEIVE_SMS:
					if (intent.getExtras() != null && intent.getExtras().getString(Constants.Extra.ADDRESS, "").equalsIgnoreCase(address)) {
						recyclerView.smoothScrollToPosition(0);
					}
					break;
			}
		}
	};
	//</editor-fold>

	//<editor-fold desc="Listeners">
	@OnClick(R.id.sendButton)
	public void onSend() {
		Intent intent = new Intent(ThreadDetailActivity.this, TwilioApiService.class);
		intent.setAction(Constants.Action.SEND_SMS);
		intent.putExtra(Constants.Extra.TO, address);
		intent.putExtra(Constants.Extra.BODY, editText.getText().toString());
		startService(intent);

		showProgressBar(true);
	}

	MessagesAdapter.Listener listListener = new MessagesAdapter.Listener() {
		@Override
		public void onClicked(String address) {
			Timber.d("Clicked %s", address);
		}
	};
	//</editor-fold>

	//<editor-fold desc="Debug">
	private void debugSendMessage() {
		realm.executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				Date date = new Date();
				Message message = new Message();
				message.setAddress(address);
				message.setBody(editText.getText().toString());
				message.setDate(date);
				message.setId(String.format("%s%s", AppValues.getInstance().getTwilioNumber(), date.getTime()));
				message.setSmsType(Constants.SmsType.SENT);

				realm.copyToRealmOrUpdate(message);
			}
		}, new Realm.Transaction.OnSuccess() {
			@Override
			public void onSuccess() {
				Intent successIntent = new Intent(Constants.Action.SEND_SMS);
				successIntent.putExtra(Constants.Extra.SUCCESS, true);
				LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(successIntent);
			}
		});
	}
	//</editor-fold>
}
