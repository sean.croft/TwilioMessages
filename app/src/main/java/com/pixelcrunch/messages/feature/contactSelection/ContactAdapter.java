package com.pixelcrunch.messages.feature.contactSelection;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.helper.ImageHelper;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.helper.ValueHelper;
import com.pixelcrunch.messages.model.RealmContact;
import com.pixelcrunch.messages.model.RealmPhoneNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by sean on 2017-08-19.
 */

public class ContactAdapter extends RealmRecyclerViewAdapter<RealmContact, ContactAdapter.ViewHolder> {
	private Listener listener;
	private Realm realm;

	public ContactAdapter(@Nullable OrderedRealmCollection<RealmContact> data, Realm realm, boolean autoUpdate, Listener listener) {
		super(data, autoUpdate);
		this.listener = listener;
		this.realm = realm;
	}

	@Override
	public void updateData(@Nullable OrderedRealmCollection<RealmContact> data) {
		super.updateData(data);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contact, parent, false);
		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		RealmContact contact = getItem(position);
		if (!RealmHelper.isValid(contact)) {
			return;
		}

		holder.bindItem(contact, contact.getNumbers(realm), listener);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		//<editor-fold desc="View Initialization">
		@BindView(R.id.rootView) View rootView;
		@BindView(R.id.imageView) ImageView imageView;
		@BindView(R.id.nameText) TextView nameTextView;
		@BindView(R.id.numberText) TextView numberTextView;
		//</editor-fold>

		ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}

		void bindItem(final RealmContact contact, final RealmResults<RealmPhoneNumber> numbers, final Listener listener) {
			if (!RealmHelper.isValid(contact)) {
				return;
			}

			final Context context = nameTextView.getContext();

			nameTextView.setText(contact.getDisplayName());
			if (numbers.isEmpty()) {
				ImageHelper.setContactAvatar(imageView, contact, contact.getDisplayName());
			} else {
				ImageHelper.setContactAvatar(imageView, contact, numbers.get(0).getNumber());
			}

			String numberText;
			switch (numbers.size()) {
				case 0:
					numberText = "No numbers";
					break;
				case 1:
					numberText = ValueHelper.formatPhoneNumber(numbers.get(0).getNumber());
					break;
				default:
					numberText = String.format("%s (+%s more)", ValueHelper.formatPhoneNumber(numbers.get(0).getNumber()), (numbers.size() - 1));
			}
			numberTextView.setText(numberText);

			if (listener != null) {
				rootView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (numbers.size() > 1) {
							showNumberSelectDialog(context, numbers, listener);
						} else if (!numbers.isEmpty()) {
							listener.onClicked(ValueHelper.normalizePhoneNumber(numbers.get(0).getNumber()));
						}
					}
				});
			}
		}

		void showNumberSelectDialog(Context context, RealmResults<RealmPhoneNumber> numbers, final Listener listener) {
			AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
			builderSingle.setTitle(R.string.select_destination);

			final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
			for (int i = 0; i < numbers.size(); i++) {
				arrayAdapter.add(ValueHelper.formatPhoneNumber(numbers.get(i).getNumber()));
			}

			builderSingle.setCancelable(true);
			builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String selectedNumber = arrayAdapter.getItem(which);
					listener.onClicked(ValueHelper.normalizePhoneNumber(selectedNumber));
					dialog.dismiss();
				}
			});
			builderSingle.show();
		}
	}

	public interface Listener {
		void onClicked(String address);
	}
}
