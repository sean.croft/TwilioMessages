package com.pixelcrunch.messages.feature.home;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.helper.ImageHelper;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.helper.ValueHelper;
import com.pixelcrunch.messages.model.Message;
import com.pixelcrunch.messages.model.RealmContact;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by sean on 2017-08-19.
 */

public class MessageListAdapter extends RealmRecyclerViewAdapter<Message, MessageListAdapter.ViewHolder> {
	private Listener listener;
	private Realm realm;

	public MessageListAdapter(@Nullable OrderedRealmCollection<Message> data, Realm realm, boolean autoUpdate, Listener listener) {
		super(data, autoUpdate);
		setHasStableIds(true);
		this.realm = realm;
		this.listener = listener;
	}

	@Override
	public void updateData(@Nullable OrderedRealmCollection<Message> data) {
		super.updateData(data);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_list, parent, false);
		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bindItem(getItem(position), realm, listener);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		//<editor-fold desc="View Initialization">
		@BindView(R.id.rootView) View rootView;
		@BindView(R.id.imageView) ImageView imageView;
		@BindView(R.id.addressText) TextView addressText;
		@BindView(R.id.previewText) TextView previewText;
		@BindView(R.id.dateText) TextView dateText;
		//</editor-fold>

		ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}

		void bindItem(final Message message, Realm realm, final Listener listener) {
			if (!RealmHelper.isValid(message)) {
				return;
			}

			RealmContact contact = RealmContact.getContactWithNumber(realm, message.getAddress());
			if (contact == null) {
				setupViewWithoutContact(message);
			} else {
				setupViewWithContact(contact, message);
			}

			if (listener != null) {
				rootView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						listener.onClicked(message.getAddress());
					}
				});
			}
		}

		private void setupViewWithContact(RealmContact contact, Message message) {
			Context context = imageView.getContext();

			addressText.setText(contact.getDisplayName());
			previewText.setText(message.getPreview());
			dateText.setText(ValueHelper.getPrettyDate(message.getDate(), context));
			ImageHelper.setContactAvatar(imageView, contact, message.getAddress());
		}

		private void setupViewWithoutContact(Message message) {
			Context context = imageView.getContext();

			addressText.setText(ValueHelper.formatPhoneNumber(message.getAddress()));
			previewText.setText(message.getPreview());
			dateText.setText(ValueHelper.getPrettyDate(message.getDate(), context));
			ImageHelper.setContactAvatar(imageView, null, message.getAddress());
		}
	}

	public interface Listener {
		void onClicked(String address);
	}
}
