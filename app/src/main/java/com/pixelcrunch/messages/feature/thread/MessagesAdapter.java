package com.pixelcrunch.messages.feature.thread;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.ImageHelper;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.helper.ValueHelper;
import com.pixelcrunch.messages.model.Message;
import com.pixelcrunch.messages.model.RealmContact;
import com.pixelcrunch.messages.view.ColorGenerator;
import com.pixelcrunch.messages.view.SimpleShapeDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by sean on 2017-08-19.
 */

public class MessagesAdapter extends RealmRecyclerViewAdapter<Message, MessagesAdapter.ViewHolder> {
	private Listener listener;
	private RealmContact contact;
	private Realm realm;

	public MessagesAdapter(@Nullable OrderedRealmCollection<Message> data, RealmContact contact, Realm realm, boolean autoUpdate, Listener listener) {
		super(data, autoUpdate);
		this.listener = listener;
		this.contact = contact;
		this.realm = realm;
	}

	@Override
	public void updateData(@Nullable OrderedRealmCollection<Message> data) {
		super.updateData(data);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_messages, parent, false);
		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bindItem(getItem(position), contact, realm, listener);
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		//<editor-fold desc="View Initialization">
		@BindView(R.id.rootView) View rootView;
		@BindView(R.id.imageView) ImageView imageView;
		@BindView(R.id.textView) TextView textView;
		@BindView(R.id.dateText) TextView dateTextView;
		@BindView(R.id.topDateText) TextView topDateText;
		//</editor-fold>

		ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}

		void bindItem(final Message message, RealmContact contact, Realm realm, final Listener listener) {
			if (!RealmHelper.isValid(message)) {
				return;
			}

			Context context = textView.getContext();

			textView.setText(message.getBody());
			dateTextView.setText(ValueHelper.getLowestDetailDate(message.getDate(), context));

			RelativeLayout.LayoutParams dateParams = (RelativeLayout.LayoutParams) dateTextView.getLayoutParams();
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textView.getLayoutParams();

			Message previousMessage = message.getPreviousMessage(realm);

			if (!message.isWithinTime(previousMessage, (5 * 60 * 1000))) {
				topDateText.setVisibility(View.VISIBLE);
				topDateText.setText(ValueHelper.getTopDate(message.getDate(), context));
			} else {
				topDateText.setVisibility(View.GONE);
			}

			if (message.getSmsType() == Constants.SmsType.SENT) {
				imageView.setVisibility(View.GONE);

				textView.setTextColor(context.getColor(android.R.color.black));
				textView.setBackground(SimpleShapeDrawable.builder().buildRoundRect(context.getColor(android.R.color.white), context.getResources().getDimensionPixelSize(R.dimen.chat_background_radius)));

				params.setMargins(context.getResources().getDimensionPixelSize(R.dimen.list_contact_avatar_size), 0, 0, 0);
				params.removeRule(RelativeLayout.RIGHT_OF);
				params.addRule(RelativeLayout.ALIGN_PARENT_END);

				dateParams.removeRule(RelativeLayout.ALIGN_LEFT);
				dateParams.addRule(RelativeLayout.ALIGN_RIGHT, R.id.textView);
			} else {
				if (message.fromSameSender(previousMessage)) {
					imageView.setVisibility(View.INVISIBLE);
				} else {
					imageView.setVisibility(View.VISIBLE);
					ImageHelper.setContactAvatar(imageView, contact, message.getAddress());
				}

				textView.setTextColor(context.getColor(android.R.color.white));
				textView.setBackground(SimpleShapeDrawable.builder().buildRoundRect(ColorGenerator.DEFAULT.getColor(message.getAddress()), context.getResources().getDimensionPixelSize(R.dimen.chat_background_radius)));

				params.setMargins(0, 0, context.getResources().getDimensionPixelSize(R.dimen.list_contact_avatar_size), 0);
				params.addRule(RelativeLayout.RIGHT_OF, R.id.imageView);
				params.removeRule(RelativeLayout.ALIGN_PARENT_END);

				dateParams.removeRule(RelativeLayout.ALIGN_RIGHT);
				dateParams.addRule(RelativeLayout.ALIGN_LEFT, R.id.textView);
			}

			textView.setLayoutParams(params);
			dateTextView.setLayoutParams(dateParams);

			if (listener != null) {
				rootView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						listener.onClicked(message.getAddress());
					}
				});
			}
		}

		@OnClick(R.id.textView)
		public void onTextClicked() {
			dateTextView.setVisibility(dateTextView.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
		}
	}

	public interface Listener {
		void onClicked(String address);
	}
}
