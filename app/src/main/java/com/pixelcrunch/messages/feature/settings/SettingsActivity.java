package com.pixelcrunch.messages.feature.settings;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.pixelcrunch.messages.PixelActivity;
import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.helper.AppValues;
import com.pixelcrunch.messages.helper.ValueHelper;

import net.steamcrafted.materialiconlib.MaterialMenuInflater;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sean1 on 10/16/2017.
 */

public class SettingsActivity extends PixelActivity {
	//<editor-fold desc="View Initialization">
	@BindView(R.id.toolbar) Toolbar toolbar;

	@BindView(R.id.accountSidLayout) TextInputLayout sidLayout;
	@BindView(R.id.accountSidEditText) EditText sidText;

	@BindView(R.id.authTokenLayout) TextInputLayout authLayout;
	@BindView(R.id.authTokenEditText) EditText authText;

	@BindView(R.id.numberLayout) TextInputLayout numberLayout;
	@BindView(R.id.phoneNumberText) EditText twilioNumberText;
	//</editor-fold>

	//<editor-fold desc="Lifecycle">
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		ButterKnife.bind(this);

		setupToolbar();
		setupViews();
	}

	//</editor-fold>

	private void setupToolbar() {
		setSupportActionBar(toolbar);

		if (getSupportActionBar() == null) {
			return;
		}

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(R.string.settings);
	}

	private void setupViews() {
		sidText.setText(AppValues.getInstance().getAccountSid());
		authText.setText(AppValues.getInstance().getAuthToken());
		twilioNumberText.setText(AppValues.getInstance().getTwilioNumber());
	}

	//<editor-fold desc="Option Menu">
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MaterialMenuInflater.with(this)
				.setDefaultColorResource(android.R.color.white)
				.inflate(R.menu.settings_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_save) {
			AppValues.getInstance().setAccountSid(sidText.getText().toString().trim());
			AppValues.getInstance().setAuthToken(authText.getText().toString().trim());
			AppValues.getInstance().setTwilioNumber(ValueHelper.normalizePhoneNumber(twilioNumberText.getText().toString().trim()));

			Snackbar.make(toolbar, "Settings Saved", Snackbar.LENGTH_SHORT).show();
			setupViews();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	//</editor-fold>

	//<editor-fold desc="Listeners">

	//</editor-fold>
}
