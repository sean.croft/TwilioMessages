package com.pixelcrunch.messages.feature.contactSelection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.pixelcrunch.messages.PixelActivity;
import com.pixelcrunch.messages.R;
import com.pixelcrunch.messages.feature.thread.ThreadDetailActivity;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.ValueHelper;
import com.pixelcrunch.messages.model.RealmContact;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;
import net.steamcrafted.materialiconlib.MaterialIconView;
import net.steamcrafted.materialiconlib.MaterialMenuInflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by sean1 on 10/21/2017.
 */

public class ContactSelectionActivity extends PixelActivity {
	//<editor-fold desc="View Initialization">
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.recyclerView) RecyclerView recyclerView;
	@BindView(R.id.editText) EditText editText;
	@BindView(R.id.phoneSwitchButton) MaterialIconView phoneSwitch;
	//</editor-fold>

	ContactAdapter adapter;
	boolean hasPhoneNumber = false;

	//<editor-fold desc="Lifecycle">
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_selection);
		ButterKnife.bind(this);

		setupToolbar();
		editText.setOnEditorActionListener(enterListener);
		setupRecyclerView();
	}
	//</editor-fold>

	private void setupToolbar() {
		setSupportActionBar(toolbar);
		if (getSupportActionBar() == null) {
			return;
		}

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(R.string.new_conversation);
	}

	private void setupRecyclerView() {
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		adapter = new ContactAdapter(RealmContact.getContacts(realm), realm, true, listListener);
		recyclerView.setAdapter(adapter);
	}

	private void changeAdapterFilter(String searchText) {
		adapter.updateData(RealmContact.getContacts(realm, searchText));
	}

	private void goToContact(String address) {
		Intent intent = new Intent(ContactSelectionActivity.this, ThreadDetailActivity.class);
		intent.putExtra(Constants.Extra.ADDRESS, address);
		intent.putExtra(Constants.Extra.PREP_SEND, true);
		startActivity(intent);
	}

	//<editor-fold desc="Option Menu">
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MaterialMenuInflater.with(this)
				.setDefaultColorResource(android.R.color.white)
				.inflate(R.menu.contact_select_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem send = menu.findItem(R.id.action_send);
		send.setVisible(hasPhoneNumber);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
			case R.id.action_send:
				goToContact(ValueHelper.normalizePhoneNumber(editText.getText().toString().trim()));
				break;
		}

		return super.onOptionsItemSelected(item);
	}
	//</editor-fold>

	//<editor-fold desc="Listeners">
	TextView.OnEditorActionListener enterListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

			if (actionId == EditorInfo.IME_ACTION_DONE) {
				String number = editText.getText().toString();
				if (ValueHelper.isValidNumber(number)) {
					goToContact(number);
					return true;
				}
				return false;
			}
			return false;
		}
	};

	@OnTextChanged(R.id.editText)
	protected void onTextChanged(CharSequence text) {
		String search = text.toString();
		changeAdapterFilter(search);

		hasPhoneNumber = ValueHelper.isValidNumber(search);
		invalidateOptionsMenu();
	}

	boolean isPhoneToggle = false;

	@OnClick(R.id.phoneSwitchButton)
	public void onPhoneSwitchClicked() {
		if (isPhoneToggle) {
			editText.setInputType(InputType.TYPE_CLASS_PHONE);
			phoneSwitch.setIcon(MaterialDrawableBuilder.IconValue.KEYBOARD);
		} else {
			editText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
			phoneSwitch.setIcon(MaterialDrawableBuilder.IconValue.DIALPAD);
		}

		isPhoneToggle = !isPhoneToggle;
	}

	ContactAdapter.Listener listListener = new ContactAdapter.Listener() {
		@Override
		public void onClicked(String address) {
			goToContact(address);
		}
	};
	//</editor-fold>
}
