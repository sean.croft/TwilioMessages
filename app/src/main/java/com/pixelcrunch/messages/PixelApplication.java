package com.pixelcrunch.messages;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.github.tamir7.contacts.Contacts;
import com.pixelcrunch.messages.helper.ApiHelper;
import com.pixelcrunch.messages.helper.AppValues;
import com.pixelcrunch.messages.helper.RealmHelper;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by sean on 2017-08-19.
 */

public class PixelApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		Contacts.initialize(this);
		AppValues.initialize(this);
		ApiHelper.initialize();

		if (BuildConfig.DEBUG) {
			Timber.plant(new DebugCrashReportingTree());
		} else {
			Timber.plant(new CrashReportingTree());
		}

		RealmHelper.setupRealm(this);
	}

	//<editor-fold desc="Timber">
	private static class CrashReportingTree extends Timber.Tree {
		public CrashReportingTree() {
		}

		@Override
		protected void log(int priority, String tag, String message, Throwable throwable) {
			if (tag == null) {
				tag = "";
			}
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				String debugToLog = String.format("Debug: %s-%s", tag, message);
			} else if (priority == Log.ERROR) {
				String errorToLog = String.format("Error: %s-%s:%s", tag, message, throwable != null ? throwable.getMessage() : "");
			} else if (priority == Log.WARN) {
				String warningToLog = String.format("Warning: %s-%s", tag, message);
			}
		}
	}

	private static class DebugCrashReportingTree extends Timber.DebugTree {
		@Override
		protected void log(int priority, String tag, String message, Throwable throwable) {
			super.log(priority, tag, message, throwable);
			if (tag == null) {
				tag = "";
			}
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				String debugToLog = String.format("Debug: %s-%s", tag, message);
			} else if (priority == Log.ERROR) {
				String errorToLog = String.format("Error: %s-%s:%s", tag, message, throwable != null ? throwable.getMessage() : "");
			} else if (priority == Log.WARN) {
				String warningToLog = String.format("Warning: %s-%s", tag, message);
			}
		}
	}
	//</editor-fold>
}
