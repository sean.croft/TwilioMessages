package com.pixelcrunch.messages.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;

import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.model.Message;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by sean1 on 10/21/2017.
 */

public class SmsReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(final Context context, Intent intent) {
		Bundle myBundle = intent.getExtras();
		final List<Message> realmMessages = new ArrayList<>();

		if (myBundle != null) {
			Object[] pdus = (Object[]) myBundle.get("pdus");
			if (pdus == null) {
				return;
			}

			for (int i = 0; i < new SmsMessage[pdus.length].length; i++) {
				realmMessages.add(new Message(SmsMessage.createFromPdu((byte[]) pdus[i])));
			}

			if (realmMessages.isEmpty()){
				return;
			}

			final String address = realmMessages.get(0).getAddress();
			try (Realm realm = RealmHelper.setupRealm(context)) {
				realm.executeTransactionAsync(new Realm.Transaction() {
					@Override
					public void execute(Realm realm) {
						realm.copyToRealmOrUpdate(realmMessages);
					}
				}, new Realm.Transaction.OnSuccess() {
					@Override
					public void onSuccess() {
						Intent successIntent = new Intent(Constants.Action.RECEIVE_SMS);
						successIntent.putExtra(Constants.Extra.ADDRESS, address);
						successIntent.putExtra(Constants.Extra.SUCCESS, true);
						LocalBroadcastManager.getInstance(context).sendBroadcast(successIntent);
					}
				});
			}
		}
	}
}
