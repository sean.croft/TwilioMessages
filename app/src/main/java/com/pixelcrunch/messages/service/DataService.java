package com.pixelcrunch.messages.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.github.tamir7.contacts.Contact;
import com.github.tamir7.contacts.Contacts;
import com.github.tamir7.contacts.PhoneNumber;
import com.pixelcrunch.messages.helper.Constants;
import com.pixelcrunch.messages.helper.RealmHelper;
import com.pixelcrunch.messages.model.Message;
import com.pixelcrunch.messages.model.RealmContact;
import com.pixelcrunch.messages.model.RealmPhoneNumber;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by sean1 on 10/12/2017.
 */

public class DataService extends IntentService {
	//<editor-fold desc="Constructor">
	public DataService() {
		super("DataService");
	}
	//</editor-fold>

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		if (intent == null || intent.getAction() == null) {
			return;
		}

		switch (intent.getAction()) {
			case Constants.Action.GET_MESSAGES:
				getInboxMessages();
				getSentMessages();
				break;
			case Constants.Action.GET_CONTACTS:
				getContacts();
				break;
			case Constants.Action.GET_ALL_DATA:
				getInboxMessages();
				getSentMessages();
				getContacts();
				break;
			default:
		}
	}

	//<editor-fold desc="Get Device Messages">
	public static final String INBOX = "content://sms/inbox";
	public static final String SENT = "content://sms/sent";
	public static final String DRAFT = "content://sms/draft";

	private void getInboxMessages() {
		final List<Message> messages = new ArrayList<>();

		try (Cursor cursor = getContentResolver().query(Uri.parse(INBOX), null, null, null, null)) {
			if (cursor == null) {
				return;
			}

			if (cursor.moveToFirst()) {
				do {
					messages.add(new Message(cursor, Constants.SmsType.RECEIVED));
				} while (cursor.moveToNext());
			} else {
				Timber.d("Inbox is empty");
			}
		}

		try (Realm realm = RealmHelper.setupRealm(this)) {
			realm.executeTransactionAsync(new Realm.Transaction() {
				@Override
				public void execute(Realm realm) {
					realm.copyToRealmOrUpdate(messages);
				}
			});
		}
	}

	private void getSentMessages() {
		final List<Message> messages = new ArrayList<>();

		try (Cursor cursor = getContentResolver().query(Uri.parse(SENT), null, null, null, null)) {
			if (cursor == null) {
				return;
			}

			if (cursor.moveToFirst()) {
				do {
					Message message = new Message(cursor, Constants.SmsType.SENT);
					messages.add(message);
				} while (cursor.moveToNext());
			} else {
				Timber.d("Sent Messages is empty");
			}
		}

		try (Realm realm = RealmHelper.setupRealm(this)) {
			realm.executeTransactionAsync(new Realm.Transaction() {
				@Override
				public void execute(Realm realm) {
					realm.copyToRealmOrUpdate(messages);
				}
			});
		}
	}
	//</editor-fold>

	//<editor-fold desc="Get Contacts">
	private void getContacts() {
		List<Contact> contacts = Contacts.getQuery().hasPhoneNumber().find();
		final List<RealmContact> realmContacts = new ArrayList<>();
		final List<RealmPhoneNumber> realmPhoneNumbers = new ArrayList<>();
		for (Contact contact : contacts) {
			realmContacts.add(new RealmContact(contact));
			for (PhoneNumber number : contact.getPhoneNumbers()) {
				realmPhoneNumbers.add(new RealmPhoneNumber(contact.getId(), number));
			}
		}

		try (Realm realm = RealmHelper.setupRealm(getApplicationContext())) {
			realm.executeTransactionAsync(new Realm.Transaction() {
				@Override
				public void execute(Realm realm) {
					realm.copyToRealmOrUpdate(realmContacts);
					realm.copyToRealmOrUpdate(realmPhoneNumbers);
				}
			});

		}
	}
	//</editor-fold>
}
