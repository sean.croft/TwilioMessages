package com.pixelcrunch.messages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.pixelcrunch.messages.helper.RealmHelper;

import io.realm.Realm;

/**
 * Created by sean on 2017-08-19.
 */

public class PixelActivity extends AppCompatActivity {
	protected Realm realm;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		realm = RealmHelper.setupRealm(this);
	}

	@Override
	protected void onDestroy() {
		realm.close();
		super.onDestroy();
	}
}
